from  moviepy.editor  import VideoFileClip

#frames = int(clip.fps * clip.duration)
#1 input file
clip = VideoFileClip("TimeLapse.mp4")
#2 skip the initiate part if the video is started from night time
s_clip = clip.cutout(0,3)
#5am to 7pm = 14 hr, 14*3 = 52. 52/10 = 5.2 sec
#7pm to 5 am = 10 hr, 10*3/10 = 3 sec
#3 #4
daytime = 4.2
nighttime = 3
duration = clip.duration
print duration
print "fps" + str( clip.fps)
#quit

#start from 7sec
total_time = 7
day = 1
while (total_time < duration):
    str_time = daytime*day
    s_clip = s_clip.cutout(str_time,str_time + nighttime)
    total_time += 7
    day += 1
    print "str_time : " + str(str_time)
    print "total_time : " + str(total_time)
    print "iteration duration : " + str(s_clip.duration)

print "final duration : " + str(s_clip.duration)

## 5ourput file
s_clip.write_videofile("my_new_video_.mp4")

#final_clip.write_videofile('../../coolTextEffects.avi',fps=25,codec='mpeg4')